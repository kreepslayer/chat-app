export interface User {
  id?: number;
  userName?: string;
  displayName?: string;
  avatarURL?: string;
  password?: string;
}
